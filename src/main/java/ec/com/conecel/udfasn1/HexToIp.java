/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author mauricio.perez
 */
public final class HexToIp extends UDF {

    public Text evaluate(final Text text) {
        if (text == null || text.toString().equals("")) { return null; }
        StringBuilder str = new StringBuilder();
        char[] cadena = text.toString().substring(4).toCharArray();

        if (text.toString().substring(4,6).equals("80")) {
            int pairs = Integer.parseInt(text.toString().substring(2,4), 16);
            int data = pairs/6;
            int count = 0;
            while (count < data) {
                for (int start = count * 12 + 4; start < count * 12 + 11; start += 2) {
                    String pair = cadena[start] + "" + cadena[start + 1];
                    str.append(Integer.parseInt(pair, 16));
                    str.append(".");
                }
                count++;
            }
        }else if (text.toString().startsWith("81")) {
            for (int i = 4; i < text.getLength(); i += 4) {
                String b = cadena[i] + "" + cadena[i + 1] + "" + cadena[i + 2] + "" + cadena[i + 3];
                str.append(b);
                str.append(":");
            }
        }else{
            int pairs = Integer.parseInt(text.toString().substring(2, 4), 16) - 2;
            int data = pairs / 6;
            int count = 0;
            cadena = text.toString().substring(8).toCharArray();
            while (count < data) {
                for (int start = count * 12 + 4; start < count * 12 + 11; start += 2) {
                    String pair = cadena[start] + "" + cadena[start + 1];
                    str.append(Integer.parseInt(pair, 16));
                    str.append(".");
                }
                count++;
            }
        }

        return new Text(str.toString().substring(0, str.length() - 1));
    }

}
