package ec.com.conecel.udfasn1;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

public final class Reverse extends UDF {
    public Text evaluate(final Text data) {
        if (data == null || data.toString().equals("")) { return null; }
        // Extraemos el string que vamos a procesar
        String str = data.toString().substring(4);
        // Calculamos la longitud del string en base a las etiquetas
        int l = Integer.parseInt(data.toString().substring(2,4), 16) * 2;
        // Inicializamos operadores
        String inverted = "";
        char tmp = '\0';
        int counter = 1;

        for (int i = 0; i < l; i++) {
            if (counter != 2) {
                if (str.charAt(i) != 'f') {
                    tmp = str.charAt(i);
                    counter++;
                }
            } else {
                if (str.charAt(i) != 'f') {
                    inverted += str.charAt(i);
                    inverted += tmp;
                    counter--;
                }
            }
        }
        
        if (counter == 2) {
            inverted += tmp;
        }

        return new Text(inverted);
    }
}