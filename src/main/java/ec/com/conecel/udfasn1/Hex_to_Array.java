/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author mauricio.perez
 */
public final class Hex_to_Array extends UDF {
    
public Text evaluate(final Text texto, int etiqueta) {
       if (texto == null) {
            return null;
        }
        char[] cadena = texto.toString().toCharArray();
        String resultado = "";
        int compara = 0;
        int a = Integer.parseInt(cadena[2] + "" + cadena[3], 16)*2+4;
        int inicia = 0;
        if (Integer.parseInt(cadena[4]+""+cadena[5]) == 30 )
        {
        inicia = 8;
        }else
        {
        inicia = 10;
        }
        
        for (int i= inicia; compara < a;) {
            int cabecera = Integer.parseInt(cadena[i] + "" + cadena[i + 1]);
            int cantidad = Integer.parseInt(cadena[i + 2] + "" + cadena[i + 3], 16);
            String texto_procesar = texto.toString().substring(i + 4, i + (cantidad * 2) + 4);
            if (etiqueta == 82 && etiqueta == cabecera) {
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta == cabecera && etiqueta == 83) {
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta == cabecera && etiqueta == 84) {
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta == cabecera && etiqueta == 85) {
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta == cabecera && etiqueta == 86) {
                try {
                    String formato = texto_procesar.substring(0,11)+"-"+texto_procesar.substring(14,18);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddhhmmssz");
                    Date parsedDate = dateFormat.parse(formato);
                    StringBuilder ffl = new StringBuilder();
                    ffl.append(parsedDate.getTime());
                    resultado = ffl.toString();
                } catch (ParseException ex) {
                    Logger.getLogger(Hex_to_Array.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (etiqueta == cabecera && etiqueta == 87) {
                int failure_h_c = Integer.parseInt(texto_procesar, 16);
                if (failure_h_c == 0) {
                    resultado = "FALSE";
                } else {
                    resultado = "TRUE";
                }
            }
            i = i + (cantidad * 2) + 4;
            compara = i;
            if (!resultado.equals("")) {
                compara = a;
            }
        }
        return new Text(resultado);
    }
}