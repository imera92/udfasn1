/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author jcedeno
 */
public class OS_timestamp extends UDF {

    public Long evaluate(Text s) {
        try {
            if (s == null || s.toString().equals("")) { return null; }
            // append a leading 0 if needed
            String str;
            // Create a new StringBuilder.
            StringBuilder builder = new StringBuilder();
            if (s.getLength() % 2 == 1) {
                str = "0" + s.toString();
            } else {
                str = s.toString();
            }
            for (int i = 0; i < str.length(); i += 2) {
                if (i == 12){
                    try {
                        byte[] bytes = Hex.decodeHex(str.substring(i, i + 2).toCharArray());
                        builder.append(new String(bytes));
                    } catch (DecoderException ex) {
                        // invalid character present, return null
                        return null;
                    }
                }
                else
                    builder.append(str.substring(i, i + 2));
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddhhmmssz");
            Date parsedDate = dateFormat.parse(builder.toString());
            //Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            
            return parsedDate.getTime();
               
            //return timestamp;
        } catch (ParseException ex) {
            // invalid character present, return null
                        return null;
        }
    }
}

