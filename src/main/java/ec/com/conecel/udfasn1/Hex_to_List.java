/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author mauricio.perez
 */
public final class Hex_to_List extends UDF {

    public Text evaluate(final Text text, String etiqueta) {
        if (text == null || text.toString().equals("")) { return null; }

        char[] cadena = text.toString().toCharArray();
        String resultado = "";
        int compara = 0;
        int a = 0;
        int inicia = 0;
        if ((cadena[4] + "" + cadena[5]).equals("82")) {
            a = Integer.parseInt(text.toString().substring(6, 10), 16) * 2 + 4;
            inicia = 14;
        } else if ((cadena[4] + "" + cadena[5]).equals("81")) {
            a = Integer.parseInt(text.toString().substring(6, 8), 16) * 2 + 4;
            inicia = 12;
        } else {
            a = Integer.parseInt(text.toString().substring(4, 6), 16) * 2 + 4;
            inicia = 10;
        }
        String texto_procesar = "";
        for (int i = inicia; compara < a;) {
            String cabecera = cadena[i] + "" + cadena[i + 1];
            System.out.println("cabecera = " + cabecera);
            int cantidad = Integer.parseInt(cadena[i + 2] + "" + cadena[i + 3], 16);
            System.out.println("cantidad = " + cantidad);
            texto_procesar = text.toString().substring(i + 4, i + (cantidad * 2) + 4);

            if (etiqueta.equals("81") && etiqueta.equals(cabecera)) {   //rating Group 
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta.equals("82") && etiqueta.equals(cabecera)) {   //chargingRuleBaseName
                resultado = texto_procesar;
            }//83 no se usa
            if (etiqueta.equals("84") && etiqueta.equals(cabecera)) {   //localSequenceNumber
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            if (etiqueta.equals("85") && etiqueta.equals(cabecera)) {   //timeOfFirstUsage
                   try {
                    String formato = texto_procesar.substring(0,11)+"-"+texto_procesar.substring(14,18);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddhhmmssz");
                    Date parsedDate = dateFormat.parse(formato);
                    StringBuilder ffl = new StringBuilder();
                    ffl.append(parsedDate.getTime());
                    resultado = ffl.toString();
                } catch (ParseException ex) {
                    Logger.getLogger(Hex_to_Array.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (etiqueta.equals("86") && etiqueta.equals(cabecera)) {   //timeOfLastUsage
                   try {
                    String formato = texto_procesar.substring(0,11)+"-"+texto_procesar.substring(14,18);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddhhmmssz");
                    Date parsedDate = dateFormat.parse(formato);
                    StringBuilder ffl = new StringBuilder();
                    ffl.append(parsedDate.getTime());
                    resultado = ffl.toString();
                } catch (ParseException ex) {
                    Logger.getLogger(Hex_to_Array.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (etiqueta.equals("87") && etiqueta.equals(cabecera)) {   //timeUsage 
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }            
            if (etiqueta.equals("91") && etiqueta.equals(cabecera)) {   //serviceIdentifier
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
if (etiqueta.equals("8c") && etiqueta.equals(cabecera)) {   //timeUsage 
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
if (etiqueta.equals("8d") && etiqueta.equals(cabecera)) {   //timeUsage 
                resultado = String.valueOf(Integer.parseInt(texto_procesar, 16));
            }
            i = i + (cantidad * 2) + 4;
            compara = i;
            if (!resultado.equals("")) {
                compara = a;
            }
        }
        return new Text(resultado);
    }

}
