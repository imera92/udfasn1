/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author mauricio.perez
 */
public final class GMT extends UDF {

    public Text evaluate(final Text hex) {
        if (hex == null || hex.toString().equals("")) { return null; }
        int i = Integer.parseInt(hex.toString(), 16);
        String bin = Integer.toBinaryString(i);
        StringBuilder srt = new StringBuilder();

        bin = ("00000000" + bin).substring(bin.length());
        srt.append(bin.substring(5));
        srt.append(bin.substring(0, 4));
        srt.insert(0, '0');
        
        int decimal = Integer.parseInt(srt.toString(), 2);
        String hexStr = Integer.toString(decimal, 16);
        if (hex.charAt(0)=='0'){
         hexStr = "-"+hexStr;   
        }
        return new Text(hexStr);
    }
}
