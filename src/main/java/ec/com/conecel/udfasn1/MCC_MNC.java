/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.conecel.udfasn1;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

/**
 *
 * @author mauricio.perez
 */
public class MCC_MNC extends UDF {

    public Text evaluate(final Text text, int tipo) {
        if (text == null || text.toString().equals("")) { return null; }
        char[] cadena = text.toString().toCharArray();
        String resultado = "";
        StringBuilder str = new StringBuilder();
        if (tipo == 1) {
            str.append(cadena[1]);
            str.append(cadena[0]);
            str.append(cadena[3]);
            resultado = str.toString();
        } else {
            str.append(cadena[5]);
            str.append(cadena[4]);
            if (!Character.isAlphabetic('F')) {
                str.append(cadena[2]);
            }
            resultado = str.toString();

        }
        return new Text(resultado);
    }

}
